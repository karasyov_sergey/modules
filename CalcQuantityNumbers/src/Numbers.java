import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Класс, предназначенный для
 * расчета количество чисел введенного
 * пользователем натурального числа
 *
 * @author KSO 17ИТ17
 */
public class Numbers {
    private static Scanner scanner = new Scanner(System.in);

    private static final String ENTER_NUMBER = "Введите натуральное число (0 не входит)" +
            ", не превосходящее 10 в 9 степени";

    private static final String RANGE_NOT_HAVE_NUMBER = "Число не находится в" +
            " необходимом диапазоне";

    private static final String QUANTITY_OF_NUMBERS = "количество чисел" +
            " введенного числа %d = %d%n";

    private static final String EXCEPTION_MESSAGE = "Введены некорректные данные/число" +
            " слишком большое\n(см. подсказку выше)";

    private static final int MAX_NUMBER_OF_RANGE = 1_000_000_000;

    public static void main(String[] args) {
        System.out.println(ENTER_NUMBER);
        try {
            long number = inputNumber();
            long quantityNumbers = getQuantityNumbers(number);
            System.out.printf(QUANTITY_OF_NUMBERS, number, quantityNumbers);
        } catch (InputMismatchException e) {
            System.out.println(EXCEPTION_MESSAGE);
        }
    }

    /**
     * Метод, который предоставляет
     * возможность пользователю
     * ввести натуральное число с клавиатуры.
     * Если значение числа превышает 10 в 9 степени
     * или число отрицательное, то предоставится
     * возможность заново ввести число.
     *
     * @return число, которое находится
     * в заданном диапазоне
     */
    private static long inputNumber() {
        long inputData = scanner.nextInt();
        while (!(inputData > 0 && inputData < MAX_NUMBER_OF_RANGE)) {
            System.out.println(RANGE_NOT_HAVE_NUMBER);
            inputData = scanner.nextInt();
        }
        return inputData;
    }

    /**
     * Метод, который возвращает
     * количество чисел пришедшего
     * натурального числа.
     *
     * @param number натуральное число
     * @return количество чисел натурального числа
     */
    public static long getQuantityNumbers(long number) {
        if (number <=0){
            throw new NumberFormatException();
        }
        return Long.toString(number).length();
    }
}

