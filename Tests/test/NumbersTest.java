import org.junit.Test;

import static org.junit.Assert.*;

public class NumbersTest {

    @Test
    public void getQuantityNumbersOne() {
        assertEquals(1, Numbers.getQuantityNumbers(5));
    }

    @Test
    public void getQuantityNumbersTwo() {
        assertEquals(2, Numbers.getQuantityNumbers(56));
    }

    @Test
    public void getQuantityNumbersThree() {
        assertEquals(3, Numbers.getQuantityNumbers(555));
    }

    @Test
    public void getQuantityNumbersFour() {
        assertEquals(4, Numbers.getQuantityNumbers(5234));
    }

    @Test
    public void getQuantityNumbersZero() {
        try {
            assertEquals(1, Numbers.getQuantityNumbers(0));
        } catch (NumberFormatException ignored) {

        }
    }

    @Test
    public void getQuantityNumbersNegative() {
        try {
            assertEquals(4, Numbers.getQuantityNumbers(-5345));
        } catch (NumberFormatException ignored) {

        }
    }
}